// Checks if sequelize connection with database was successful
async function checkDbConnection(){
    try{
        await sequelize.authenticate();
        console.log("Connection successful")
    }catch( err){ console.log(err)}
}

// Languages table queries

async function getLanguages () {
    try{
        return await tableLanguages.findAll({raw: true});
    }catch ( err){
        console.log(err);
    }
}

async function getLanguageById ( idToFind) {
    try {
        return await tableLanguages.findOne( {raw: true, where: { id: idToFind}})
    } catch (err) {
        console.log(err)
    }
}

async function getLanguageByName ( nameToFind) {
    try {
        return await tableLanguages.findAll( {raw: true, where : {  name: { [Op.like]: nameToFind } }})
    } catch (err) {
        console.log(err);
    }
}

async function getProjectById(id) {
    try {
        const WHERE_CLAUSE = {projectId: id};

        // Get project by id
        const project = tableProjects.findOne({where: {id: id}});

        // Get project images by projectId
        const projectImages = tableProjectsImages.findAll({where: WHERE_CLAUSE})

        // Get project languages by projectId
        const projectLanguages = tableProjectsLanguages.findAll({where: WHERE_CLAUSE})

        // Get project tags by projectId
        const projectTags = tableProjectsTags.findAll({where: WHERE_CLAUSE})

        const results = await Promise.all([project, projectImages, projectLanguages, projectTags])

        // Build response DTO
        const response = results[0].dataValues
        const subfields = ['images', 'languages', 'tags']

        // Rearrange date to ISO format
        response.date = new Date(response.date).toISOString();

        // Create images, tags and languages fields that were not fetch in db
        subfields.forEach((subfield, index) => {
            const subfieldArray = results[index+1];
            response[subfield] = []

            if(subfieldArray.length === 0) return;
            
            subfieldArray.forEach(iterator => response[subfield].push(iterator.dataValues))
        });
        
        return response;
    } catch (error) {
        console.error(error);
        return null;
    }
}

async function deleteProjects(projectIds){
    if ( !Array.isArray( projectIds) )
        projectIds = [projectIds]
    try {
        await tableProjects.destroy({
            where: {
                id: { [Op.in]: projectIds}
            },
        })

        return true
    } catch (error) {
        console.log(error)
    }
    return false
}

async function updateProject(project) {
    try {
        await sequelize.transaction(async (t) => {
            await tableProjects.destroy({where: {id: project.id}},{transaction: t})

            // Create project
            const newProject = await tableProjects.create({
                name: project.name,
                date: project.date,
                repository: project.repository ?? null,
                description: project.description ?? null,
                state: project.state,
                user_id: project.userId
            }, {transaction: t})
    
            // If it has tags, create project_tags
            if(project.tags && project.tags.length > 0) {
                project.tags = project.tags.map(tag => { 
                    return {'tagId': tag, 'projectId': newProject.id}
                })
                
                await tableProjectsTags.bulkCreate(project.tags, { transaction: t })
            }
    
            // If it has languages, create project_languages
            if(project.languages && project.languages.length > 0) {
                project.languages = project.languages.map(language => {
                    return {'projectId': newProject.id, 'languageId': language}
                })

                await tableProjectsLanguages.bulkCreate(project.languages, {transaction: t})
            }
    
            // If it has images, create project_images
            if(project.images && project.images.length > 0) {
                project.images = project.images.map(image => {
                    return {'projectId': newProject.id, 'path': image}
                })
                await tableProjectsImages.bulkCreate(project.images, {transaction: t})
            }
        });
        return project;    
    } catch (error) {
        console.error(error);
    }
}

async function createProject(project) {
    try {
        await sequelize.transaction(async (t) => {
            // Create project
            const newProject = await tableProjects.create({
                name: project.name,
                date: project.date,
                repository: project.repository ?? null,
                description: project.description ?? null,
                state: project.state,
                user_id: project.userId
            }, {transaction: t})
    
            // If it has tags, create project_tags
            if(project.tags && project.tags.length > 0) {
                project.tags = project.tags.map(tag => { 
                    return {'tagId': tag, 'projectId': newProject.id}
                })
                
                await tableProjectsTags.bulkCreate(project.tags, { transaction: t })
            }
    
            // If it has languages, create project_languages
            if(project.languages && project.languages.length > 0) {
                project.languages = project.languages.map(language => {
                    return {'projectId': newProject.id, 'languageId': language}
                })

                await tableProjectsLanguages.bulkCreate(project.languages, {transaction: t})
            }
    
            // If it has images, create project_images
            if(project.images && project.images.length > 0) {
                project.images = project.images.map(image => {
                    return {'projectId': newProject.id, 'path': image}
                })
                await tableProjectsImages.bulkCreate(project.images, {transaction: t})
            }
        });     
        return project
    } catch (error) {
        console.error(error);
    }
}

async function insertProjectImage( projectId, imagePath){
    try {
        
        await tableProjectsImages.create({
            projectId: projectId,
            path: imagePath,
        })
        return true
    } catch (error) {
        console.log(error)
    }

    return false
}

async function getProjectLanguages ( projectId, limit = null){
    try {
        
        let languages = await tableProjectsLanguages.findAll({
            attributes: ['languageId'], 
            where: { projectId: projectId}, 
            limit: limit, 
            raw: true
        })
        if ( languages.length <= 0)
            return null

        let languagesId = []
        for( let language of languages)
            languagesId.push( language.languageId)

        return await tableLanguages.findAll({
            where : {
                id: {
                    [Op.or]: languagesId
                }
            },
            raw: true
        })


    } catch (err) {
        console.log(err)
    }
}

async function getTags(limit = null) {
    try {
        return await tableTags.findAll({
            limit: limit,
            raw: true
        });            
    } catch (error) {
        console.log(error);
        return [];
    }
}

async function getProjectTags ( projectId, limit = null){
    try {
        
        let tags = await tableProjectsTags.findAll({
            attributes: ['tagId'], 
            where: { projectId: projectId}, 
            limit: limit, 
            raw: true
        })
        if ( tags.length <= 0)
            return null

        let tagsId = []
        for( let tag of tags)
            tagsId.push( tag.tagId)

        return await tableTags.findAll({
            where : {
                id: {
                    [Op.or]: tagsId
                }
            },
            raw: true
        })


    } catch (err) {
        console.log(err)
    }
}

// Users table queries
async function getUserByEmail(email) {
    try {
        return await tableUsers.findOne( {raw: true, where: {email: email}} )
    } catch (err) {
        console.log(err);
    }
}


async function getUserById( idToFind){
    try {
        let user =  await tableUsers.findOne( {raw: true, where: {id: idToFind}})
        userDateTimeTreatment( user)
        if ( user == null)
            return {
                id: -1
            }

        return user
    } catch (err) {
        console.log(err)   
    }
}

async function createUser( userToCreate){
    try{
        const hashedPassword = await hashPassword( userToCreate.password);

        let userCreated = await tableUsers.create({
            name: userToCreate.name,
            email: userToCreate.email,
            password: hashedPassword
        }, { raw: true})
        //Since graphql does'nt have a native date/dateTime type, we declare it as a string in graphql
        //and transform the dateTime into a string with toISOString
        userDateTimeTreatment( userCreated.dataValues)

        return userCreated;
    }catch( err){
        console.log( err)
    }

}

async function updateUser( userUpdate){
    try {
        let obj = {}
        for ( key in userUpdate)
            if ( key != "id")
                obj[key] = userUpdate[key]    
                    
        await tableUsers.update(obj, {
            where: {id: userUpdate.id}
        })


        return await getUserById( userUpdate.id)
        
    } catch (err) {
        console.log(err)
    }
}

async function deleteUser(id) {
    try {
        return await tableUsers.destroy({
            where: {id: id}
        }) > 0;
    } catch (error) {
        console.log(error);
    }
}

// checks user credentials, return values: 
// user.id = -2 if user with that email is not found
// user.id = -1 if password is not correct
// User object if both email and password are correct
async function checkUserCredentials( email, password){
    try {
        let user = await tableUsers.findOne({raw: true, where: {email: email }})
        
        if ( user === null)
        return user = { id: -2}
        
        if ( await comparePassword( password, user.password) ){
            userDateTimeTreatment(user)
            return user;
        }
        
        return user = { id: -1 };
        
    } catch (err) {
        console.log(err)
    }
}

async function countUserProjects( id){
    try {
        return await tableProjects.count( { where: { user_id: id}})
    } catch (err) {
        console.log(err)
    }
}

async function getUserProjects(id, offset = 0, limit = null){
    try {
        return await tableProjects.findAll({
            where: { user_id: id},
            offset: offset,
            limit: limit,
            raw: true
        })
    } catch (err) {
        console.log(err)
    }
}

async function countUserLanguages(id){
    try {
        let userProjects = await getUserProjects(id)
        let userProjectsId = []

        userProjects.forEach((item) => {
            userProjectsId.push(item.id)
        })
        
        return await tableProjectsLanguages.count({
            distinct: true,
            col: 'languageId',
            where: { projectId: userProjectsId },
        })
        
    } catch (err) {
        console.log(err)
    }
}

function userDateTimeTreatment( user){
    if ( user != null && 'createdAt' in user && 'updatedAt' in user){
        user.createdAt = user.createdAt.toISOString() 
        user.updatedAt = user.updatedAt.toISOString()

    }
}

// password hashing
async function hashPassword( password, saltRounds = 10){
    try {
        const salt = await bcrypt.genSalt(saltRounds)
        return await bcrypt.hash(password, salt)
    } catch (err) {
        console.log(err)
    }
}

async function comparePassword( password, hashedPassword){
    try {
        return await bcrypt.compare(password, hashedPassword)  
    } catch (err) {
        console.log( err)
    }
}

require('dotenv/config');

const bcrypt= require('bcrypt');
const {Sequelize, Op} = require('sequelize');

const sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_USERNAME, process.env.DB_SECRET, {
    dialect: "mysql",
    port: 3306,
})

checkDbConnection()

module.exports = { 
    getLanguages,
    getLanguageById,
    getLanguageByName,
    countUserLanguages,

    getProjectLanguages,
    getProjectTags,
    createProject,
    updateProject,
    getProjectById,
    deleteProjects,
    insertProjectImage,

    getTags,

    getUserByEmail,
    deleteUser,
    getUserById,
    createUser,
    checkUserCredentials,
    updateUser,

    countUserProjects,
    getUserProjects,
    
    sequelize,
};

const tableLanguages = require('./languages')
const tableUsers = require('./users')
const tableProjects = require('./projects')
const tableProjectsLanguages = require('./projectLanguages')
const tableProjectsImages = require('./projectImages')
const tableTags = require('./tags')
const tableProjectsTags = require('./projectTags')