const {Sequelize} = require('sequelize');
const {sequelize} = require('./db');

const tableLanguages = sequelize.define('language', {
    id:{
        type: Sequelize.INTEGER(4),
        primaryKey: true,
        allowNull: true
    },
    name: Sequelize.STRING(128),
    imagePath: Sequelize.STRING(128),
    
}, {timestamps: false});
tableLanguages.sync();

module.exports = tableLanguages;