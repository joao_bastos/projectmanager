const { Sequelize } = require('sequelize')
const { sequelize } = require('./db')

const tableProjects = require('./projects')

const tableProjectsImages = sequelize.define('project_images', {
    path: {
        type: Sequelize.STRING(256),
        primaryKey: true,
        allowNull: false
    },
}, {timestamps: false})


tableProjectsImages.belongsToMany(tableProjects, {
    through: tableProjectsImages,
    foreignKey: 'projectId',
    primaryKey: true,
    allowNull: false
})

tableProjectsImages.sync()

module.exports = tableProjectsImages;