const { Sequelize } = require('sequelize');
const { sequelize } = require('./db');  
const tableUsers = require('./users');

const stateValues = ['Por iniciar', 'Em desenvolvimento', 'Terminado']

const tableProjects = sequelize.define('projects', {
    id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
    },
    name: { type: Sequelize.STRING(32), allowNull: false},
    description: Sequelize.STRING(1024),
    state: { type: Sequelize.ENUM(stateValues), allowNull: false },
    date: { type: Sequelize.DATEONLY(), allowNull: false},
    repository: Sequelize.STRING(256),
}, {timestamps: false})

tableProjects.belongsTo(tableUsers, { foreignKey: 'user_id'})
tableProjects.sync()

module.exports = tableProjects