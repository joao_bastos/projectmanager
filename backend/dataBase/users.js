const { Sequelize } = require('sequelize');
const { sequelize } = require('./db');  

const tableUsers = sequelize.define('users', {
    id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
    },
    name: { type: Sequelize.STRING(64), allowNull: false},
    email: { type: Sequelize.STRING(128), allowNull: false},
    password: { type: Sequelize.STRING(128), allowNull: false},
    profile_description: Sequelize.STRING(512),
    profile_image: Sequelize.STRING(32),
    profile_public: Sequelize.BOOLEAN(),

}, {timestamps: true})

tableUsers.sync()


module.exports = tableUsers 