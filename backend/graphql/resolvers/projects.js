const { countUserProjects,
    getUserProjects,
    createProject,
    getProjectById, 
    updateProject, 
    deleteProjects, 
    insertProjectImage
} = require('../../dataBase/db')

let projectsResolver = {
    countUserProjects(obj){
        return countUserProjects(obj.id)
    },
    getUserProjects(obj){
        return getUserProjects(obj.id, obj.offset ?? 0, obj.limit ?? null)
    },
    createProject(obj) {
        return createProject(obj.project) ?? null
    },
    updateProject(obj) {
        return updateProject(obj.project) ?? null
    },
    getProjectById(obj) {
        return getProjectById(obj.id) ?? null
    },
    deleteProjects(obj){
        return deleteProjects(obj.projectIds)
    },
    insertProjectImage(obj){
        return insertProjectImage(obj.projectId, obj.path)
    }
}

module.exports = projectsResolver
