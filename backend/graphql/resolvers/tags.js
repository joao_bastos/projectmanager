const { getProjectTags, getTags } = require('../../dataBase/db')

let tagsResolver = {
    getProjectTags(obj){
        return getProjectTags(obj.id, obj.limit ?? null)
    },
    getTags() {
        return getTags();
    }
}

module.exports = tagsResolver