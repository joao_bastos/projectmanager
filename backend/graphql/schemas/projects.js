const projectSchema = `
type Project {
    id: ID!,
    user_id: String,
    name: String,
    description: String,
    state: String,
    date: String,
    repository: String
}

type ProjectImage {
    projectId: ID,
    path: String
}

type ProjectLanguage {
    id: Int,
    languageId: Int
    projectId: ID
}

type ProjectTag {
    id: Int,
    tagId: Int
    projectId: ID
}

type ProjectOutput {
    id: ID!,
    user_id: String,
    name: String,
    description: String,
    state: String,
    date: String,
    repository: String,
    images: [ProjectImage],
    tags: [ProjectTag],
    languages: [ProjectLanguage]
}

input createProjectInput {
    id: ID
    date: String!
    repository: String
    description: String
    images: [String]
    languages: [Int]
    name: String!
    state: String!
    tags: [Int]
    userId: ID!
}`

module.exports = projectSchema