require('dotenv/config');
const express = require('express');
const cors = require('cors');
require('./dataBase/db');
const { merge } = require('lodash');

const app = express();
app.use( cors() );


const { buildSchema} = require('graphql');
const { graphqlHTTP } = require('express-graphql');

app.listen(process.env.SERVER_PORT);


let mainSchema = `
type Query {
  getAllLanguages : [Language]
  getLanguageById( id: Int) : Language
  getLanguageByName( name: String ) : [Language]
  getProjectLanguages( id: ID, limit: Int): [Language]
  
  getTags : [Tag]
  getProjectTags( id: ID, limit: Int): [Tag]

  getUserById( id: ID) : User
  getUserByEmail(email: String): User
  checkUserCredentials( email: String, password: String): User

  countUserProjects( id: ID): Int
  getUserProjects(id: ID, offset: Int, limit: Int): [Project]
  getProjectById(id: ID!): ProjectOutput

  countUserLanguages(id: ID): Int
}

type Mutation{
  createUser( user: createUserInput): User
  updateUser( userUpdate: updateUserInput): User
  deleteUser( id: ID): Boolean

  createProject (project: createProjectInput): Project
  updateProject (project: createProjectInput): Project
  deleteProjects ( projectIds: [String] ): Boolean
  insertProjectImage( projectId: String, path: String): Boolean
}

`



mainSchema = mainSchema.concat( 
  require('./graphql/schemas/languages'),
  require('./graphql/schemas/users'),
  require('./graphql/schemas/projects'),
  require('./graphql/schemas/tags')
  );

let schema = buildSchema(mainSchema);

let resolvers =  merge( {}, 
  require('./graphql/resolvers/languages'),
  require('./graphql/resolvers/users'),
  require('./graphql/resolvers/projects'),
  require('./graphql/resolvers/tags')
  )


app.use(
  '/graphql',
  graphqlHTTP({
    schema,
    rootValue: resolvers,
    graphiql: true,
  }),
);
