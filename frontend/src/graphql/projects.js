import gql from "graphql-tag"

const COUNT_USER_PROJECTS = id => gql`
query {
    countUserProjects(id : "${id}")
}
`

const GET_PROJECT_BY_ID = id => gql`
query {
    getProjectById(id: "${id}") {
        id
        user_id,
        name,
        description,
        state,
        date,
        repository,
        images {
            projectId
            path
        },
        tags {
            id
            tagId
        },
        languages {
            id
            languageId
        }
    } 
}
`

const CREATE_PROJECT = (project) => gql`
mutation {
    createProject(project: { 
        name: "${project.name}", 
        date: "${project.date}",
        userId: "${project.userId}",
        state: "${project.state}",
        ${project.repository ? `repository: "${project.repository}"` : ''}, 
        ${project.description ? `description: "${project.description}"` : ''},
        ${project.languages ? `languages: [${project.languages}]` : ''},
        ${project.tags ? `tags: [${project.tags}]` : ''},
        ${project.images ? `images: ${JSON.stringify(project.images)}` : ''}
        } ) {
            name,
            user_id,
            state,
            date,
            description,
            repository
        }
}
`

const UPDATE_PROJECT = (project) => gql`
mutation {
    updateProject(project: { 
        id: "${project.id}",
        name: "${project.name}", 
        date: "${project.date}",
        userId: "${project.userId}",
        state: "${project.state}",
        ${project.repository ? `repository: "${project.repository}"` : ''}, 
        ${project.description ? `description: "${project.description}"` : ''},
        ${project.languages ? `languages: [${project.languages}]` : ''},
        ${project.tags ? `tags: [${project.tags}]` : ''},
        ${project.images ? `images: ${JSON.stringify(project.images)}` : ''}
        } ) {
            name,
            user_id,
            state,
            date,
            description,
            repository
        }
}
`

const GET_USER_PROJECTS = ( id, offset = 0, limit= null) => gql`
query {
    getUserProjects(id: "${id}", offset: ${offset}, limit: ${limit}){
        id
        user_id
        name
        description
        state
        date
        repository  
    }
}
`

const DELETE_PROJECTS = ( stringFormatted ) => gql`
mutation{
    deleteProjects(projectIds: ${stringFormatted})
}
`

const INSERT_PROJECT_IMAGE = ( projectId, path) => gql`
mutation{
    insertProjectImage(projectId: "${projectId}", path: "${path}")
}
`


export default {

    COUNT_USER_PROJECTS,
    GET_USER_PROJECTS,
    CREATE_PROJECT,
    UPDATE_PROJECT,
    GET_PROJECT_BY_ID,
    DELETE_PROJECTS,
    INSERT_PROJECT_IMAGE
}

