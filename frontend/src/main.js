import { createApp, provide, h  } from 'vue'
import App from './App.vue'
import './index.css' // tailwind file
import axios from 'axios'

//apollo client imports
import { apolloClient } from './apolloClient'
import { DefaultApolloClient } from '@vue/apollo-composable'

//font awesome imports
import {library} from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome"
import { 
  faArrowLeft,
  faPlus,
  faExclamationCircle,
  faEye, 
  faEyeSlash, 
  faSpinner,
  faRightToBracket,
  faRightFromBracket, 
  faUser,
  faAngleDown,
  faFloppyDisk,
  faTrash,
  faCheck,
  faCircleInfo,
  faCircleXmark,
  faCircleCheck,
  faAngleUp,
  faPen,
} from '@fortawesome/free-solid-svg-icons'

import  router  from './routes/router'

library.add(faExclamationCircle)
library.add(faPlus);
library.add(faEye);
library.add(faEyeSlash);
library.add(faSpinner);
library.add(faRightToBracket)
library.add(faRightFromBracket)
library.add(faUser);
library.add(faAngleDown);
library.add(faAngleUp)
library.add(faFloppyDisk)
library.add(faTrash)
library.add(faCheck)
library.add(faCircleInfo)
library.add(faCircleXmark)
library.add(faCircleCheck)
library.add(faArrowLeft)
library.add(faPen)



const app = createApp({
    setup () {
      provide(DefaultApolloClient, apolloClient)
    },
    
    render: () => h(App),
})

app.mixin({
  methods: {
    uploadImageToApi( img, apiKey, uniqueQuery){
      return axios.post('https://api.sirv.com/v2/files/upload?filename=%2FPANDORA_CODE%2F'+ uniqueQuery +'.png', 
      img, {headers: {
          Authorization: "Bearer "+ apiKey,
          "Content-Type": "application/json"
      }})  
    },
    async getSirvApiKey(){
      const clientId = 'WsqgY0T5Bk8neESXK3QHsQCMxhs';
      const clientSecret = 'Ivt4e39q3NKsk/hOb+J2QiPY7Z/oTQ4RWtVV2mCUEqhl9FA1FIpAUz+Du4fOP/oz8Sz73CjwulOzHv9DvNabXg==';

      let response = await axios.post('https://api.sirv.com/v2/token', 
      {"clientId": clientId, "clientSecret": clientSecret})
      let token = response.data?.token ?? null
      return token
    },
    createSession (user, rememberMe = false){
      
      window.localStorage.setItem('name', user.name)
      window.localStorage.setItem('email', user.email)
      window.localStorage.setItem('id', user.id)
      if ( !rememberMe ){
        let d = new Date();
        d.setDate( d.getDate() + 1)
        window.localStorage.setItem('expirationDate', d)
        
      }
    },
    getSession (){
      let expirationDate = null
      
      if ( window.localStorage.getItem('expirationDate') )
        expirationDate = new Date(window.localStorage.getItem('expirationDate'));

      if( expirationDate && Date.now() >= expirationDate.getTime() ){
          this.deleteSession()
      }
        
      return window.localStorage
    },
    deleteSession(){
      window.localStorage.clear()
    }
  }
})

app.use(router)

//create the component from FontAwesomeIcon as "font-awesome-icon"
app.component("font-awesome-icon", FontAwesomeIcon)

app.mount('#app')   

