import { createRouter, createWebHistory } from 'vue-router'

const routes = [
    {
        path: '/',
        redirect: () => {
            const authUserId = localStorage.getItem('id')
            if (!authUserId)
                return '/sessions/login';
            return '/users/'+authUserId;
        }
    },
    {
        path: '/sessions',
        component: () => import('../views/sessionView.vue'),
        children: [
            {
                name: 'login',
                path: '',
                component: () => import('../views/loginPage.vue'),
                alias: ['login']
            },
            {
                name: 'signup',
                path: 'signup',
                component: () => import('../views/signupPage.vue')
            }
        ]
    },
    {
        path: '/users/:userId/',
        component: () => import('../views/profileProjectsView.vue'),
        children: [
            {
                name: 'user',
                path: '',
                components: {
                    default: () => import('../views/userProjects.vue'),
                    navBar: () => import('../components/NavBar/navBar.vue'),
                    profile: () => import('../components/Profile/profilePack.vue')
                },
                alias: ['/users/:userId/profile'],

            },
            {
                name: 'projects-create',
                path: '/users/:userId/projects/create',
                component: () => import('../views/projectsCreateView.vue'),
                children: [
                    {
                        name: 'create',
                        path: 'create',
                        components: {
                            default: () => import('../views/userProjects.vue'),
                            navBar: () => import('../components/NavBar/navBar.vue'),
                            profile: () => import('../components/Profile/profilePack.vue')
                        },
                        alias: ['/users/:userId/projects/create'],
                    },
                ]        
            },
            {
                name: 'projects-details',
                path: '/users/:userId/projects/:projectId/details',
                component: () => import('../views/projectsDetailsView.vue'),
                children: [
                    {
                        name: 'details',
                        path: 'details',
                        components: {
                            default: () => import('../views/userProjects.vue'),
                            navBar: () => import('../components/NavBar/navBar.vue'),
                            profile: () => import('../components/Profile/profilePack.vue')  
                        },
                        alias: ['/users/:userId/projects/:projectId/details'],
                    },
                ]        
            },            
            {
                name: 'projects-update',
                path: '/users/:userId/projects/:projectId/update',
                component: () => import('../views/projectsUpdateView.vue'),
                children: [
                    {
                        name: 'update',
                        path: 'update',
                        components: {
                            default: () => import('../views/userProjects.vue'),
                            navBar: () => import('../components/NavBar/navBar.vue'),
                            profile: () => import('../components/Profile/profilePack.vue')  
                        },
                        alias: ['/users/:userId/projects/:projectId/update'],
                    },
                ]        
            },            
        ]        
    },    
    {
        
        path: '/myProfile',
        component: () => import('../views/myProfilePage.vue'),
        children: [
            {
                name: 'myProfile',
                path: '',
                components: {
                    default: () => import('../views/manageProfileView.vue'),
                    navBar: () => import('../components/NavBar/navBar.vue'),
                    
                }
            }
        ]
    },
]

const router  = createRouter( { history: createWebHistory(import.meta.url.BASE_URL), routes: routes})


export default router