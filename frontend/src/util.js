const ELEM_NOT_FOUND = -1;

const arrayPush = function(array, value) {
    if(array.indexOf(value) === ELEM_NOT_FOUND)
      array.push(value);
}

const arrayDelete = function(array, value) {
    const index = array.indexOf(value);
    if (index !== ELEM_NOT_FOUND)
      return array.splice(index, 1);
}

export default {
    arrayInsert: arrayPush,
    arrayDelete: arrayDelete
}